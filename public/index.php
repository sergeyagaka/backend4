<?php
header('Content-Type: text/html; charset=UTF-8');
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  $messages = array();
	if (!empty($_COOKIE['save'])) {
      setcookie('save', '', 100000);
    	$messages[] = 'Данные сохранены';
  	}
    $errors = array();
    $errors['field-name1'] = !empty($_COOKIE['field-name1_error']);
    $errors['field-email'] = !empty($_COOKIE['field-email_error']);
    $errors['field-date'] = !empty($_COOKIE['field-date_error']);
    $errors['radio-1'] = !empty($_COOKIE['radio-1_error']);
    $errors['radio-2'] = !empty($_COOKIE['radio-2_error']);
    $errors['supernatural'] = !empty($_COOKIE['supernatural_error']);
    $errors['field-area'] = !empty($_COOKIE['field-area_error']);
    $errors['check-1'] = !empty($_COOKIE['check-1_error']);
    $errors['1'] = !empty($_COOKIE['1_error']);
    $errors['2'] = !empty($_COOKIE['2_error']);
    $errors['3'] = !empty($_COOKIE['3_error']);

    if ($errors['field-name1']) {
      setcookie('field-name1_error', '', 100000);
      $messages[] = '<div class="error">Введите имя.</div>';
    }
    if ($errors['1']){
      setcookie('1_error', '', 100000);
      $messages[] = '<div class="error">Имя должно быть написано при помощи латинского алфавита</div>';
    } 
    if ($errors['field-email']) {
      setcookie('field-email_error', '', 100000);
      $messages[] = '<div class="error">Введите e-mail.</div>';
    }
    if ($errors['2']){
      setcookie('2_error', '', 100000);
      $messages[] = '<div class="error">Формат эл почты имеет вид: example@email.com</div>';
    } 
    if ($errors['field-date']) {
      setcookie('field-date_error', '', 100000);
      $messages[] = '<div class="error">Введите дату.</div>';
    }
    if ($errors['3']){
      setcookie('3_error', '', 100000);
      $messages[] = '<div class="error">Формат даты 02.02.2021</div>';
    } 
    if ($errors['radio-1']) {
      setcookie('radio-1_error', '', 100000);
      $messages[] = '<div class="error">Выберите пол.</div>';
    }
    if ($errors['radio-2']) {
      setcookie('radio-2_error', '', 100000);
      $messages[] = '<div class="error">Выберите кол-во конечностей.</div>';
    }
    if ($errors['supernatural']) {
      setcookie('supernatural_error', '', 100000);
      $messages[] = '<div class="error">Выберите суперспособность.</div>';
    }
    if ($errors['field-area']) {
      setcookie('field-area_error', '', 100000);
      $messages[] = '<div class="error">Напишите биографию.</div>';
    }
    if ($errors['check-1']) {
      setcookie('check-1_error', '', 100000);
      $messages[] = '<div class="error">Примите условия.</div>';
    }
  
    $values = array();

    $values['field-name1'] = empty($_COOKIE['field-name1_value']) ? '' : $_COOKIE['field-name1_value'];
    $values['field-email'] = empty($_COOKIE['field-email_value']) ? '' : $_COOKIE['field-email_value'];
    $values['field-date'] = empty($_COOKIE['field-date_value']) ? '' : $_COOKIE['field-date_value'];
    $values['radio-1'] = empty($_COOKIE['radio-1_value']) ? '' : $_COOKIE['radio-1_value'];
    $values['radio-2'] = empty($_COOKIE['radio-2_value']) ? '' : $_COOKIE['radio-2_value'];
    $values['supernatural'] = empty($_COOKIE['supernatural_value']) ? '' : $_COOKIE['supernatural_value'];
    $values['field-area'] = empty($_COOKIE['field-area_value']) ? '' : $_COOKIE['field-area_value'];
    $values['check-1'] = empty($_COOKIE['check-1_value']) ? '' : $_COOKIE['check-1_value'];
  

  	include('form.php');
}
else{

  $errors = FALSE;

  if (!preg_match("/^[-a-zA-Z]+$/",$_POST['field-name1'])){
    setcookie('1_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;//!preg_match("/^([а-яё]+|[a-z]+)$/i",
  } 
  if (!preg_match("/\b[\w\.-]+@[\w\.-]+\.\w{2,4}\b/",$_POST['field-email'])){
    setcookie('2_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  } 
  if (!preg_match("/^(\d{1,2})\.(\d{1,2})(?:\.(\d{4}))?$/",$_POST['field-date'])){
    setcookie('3_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  if (empty($_POST['field-name1'])) {
    setcookie('field-name1_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('field-name1_value', $_POST['field-name1'], time() + 365 * 24 * 60 * 60);
  }
  if (empty($_POST['field-email'])) {
    setcookie('field-email_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('field-email_value', $_POST['field-email'], time() + 365 * 24 * 60 * 60);
  }
  if (empty($_POST['field-date'])) {
    setcookie('field-date_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('field-date_value', $_POST['field-date'], time() + 365 * 24 * 60 * 60);
  }
  if (empty($_POST['radio-1'])) {
    setcookie('radio-1_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('radio-1_value', $_POST['radio-1'], time() + 365 * 24 * 60 * 60);
  }
  if (empty($_POST['radio-2'])) {
    setcookie('radio-2_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('radio-2_value', $_POST['radio-2'], time() + 365 * 24 * 60 * 60);
  }
  if (empty($_POST['supernatural'])) {
    setcookie('supernatural_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('supernatural_value', $_POST['supernatural'], time() + 365 * 24 * 60 * 60);
  }
  if (empty($_POST['field-area'])) {
    setcookie('field-area_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('field-area_value', $_POST['field-area'], time() + 365 * 24 * 60 * 60);
  }
  if (empty($_POST['check-1'])) {
    setcookie('check-1_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('check-1_value', $_POST['check-1'], time() + 365 * 24 * 60 * 60);
  }

  if ($errors) {
    header('Location: index.php');
    exit();
  }
  else {
    setcookie('field-name1_error', '', 100000);
    setcookie('1_error', '', 100000);
    setcookie('field-email_error', '', 100000);
    setcookie('2_error', '', 100000);
    setcookie('field-date_error', '', 100000);
    setcookie('3_error', '', 100000);
    setcookie('radio-1_error', '', 100000);
    setcookie('radio-2_error', '', 100000);
    setcookie('supernatural_error', '', 100000);
    setcookie('field-area_error', '', 100000);
    setcookie('check-1_error', '', 100000);
  }

    $name = $_POST['field-name-1'];
    $email = $_POST['field-email'];
    $date = $_POST['field-date'];
    $radio1 = $_POST['radio-1'];
    $radio2 = $_POST['radio-2'];
    $field4 = $_POST['supernatural'];
    $name2 = $_POST['field-area'];
    $check = $_POST['check-1'];


    //авторизация
    $user = 'u24054';
    $pass = '6175754';
    $db = new PDO('mysql:host=localhost;dbname=u24054', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
    //отправка
    try {
        $stmt = $db->prepare("INSERT INTO form (name,email,date,radio1,radio2,field4,name2,check1) VALUE (:name,:email,:date,:radio1,:radio2,:field4,:name2,:check1)");
        $stmt -> execute(['name'=>$name,'email'=>$email,'date'=>$date,'radio1'=>$radio1,'radio2'=>$radio2,'field4'=>$field4,'name2'=>$name2,'check1'=>$check]);
        echo "<script type='text/javascript'>alert('Данные отправлены:)');</script>";
    }catch (PDOException $e) {
        print('Error : ' . $e->getMessage());
        exit();
    }
    setcookie('save', '1');
    header('Location: index.php');
}
